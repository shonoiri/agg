import { Injectable }       from '@angular/core';

import { DropdownQuestion } from './model/question_dropdown';
import { QuestionBase }     from './model/question_base';
import { TextboxQuestion }  from './model/question_textbox';
import { TextareaQuestion }  from './model/question_textarea';
import { LabelQuestion }  from './model/question_label';

@Injectable()
export class QuestionService {

  getPreQuestions(readOnly, row){

  let questions: QuestionBase<any>[] = [
      new DropdownQuestion({
        readOnly: readOnly,
        value: row.role,
        values: row.roles,
        key: "comboBox"
      }),

      new TextboxQuestion({
        readOnly: readOnly,
        value: row.name,
        required: true,
        key: "label"
      })
     ];
    return questions;
  };

  getPostQuestions(readOnly, row){

    let questions: QuestionBase<any>[] = [

      new DropdownQuestion({
        readOnly: readOnly,
        value: "",
        values: row.roles,
        key: "comboBox"
      }),

      new TextboxQuestion({
        readOnly: readOnly,
        value: "",
        key: "label"
      })
     ];
    return questions;
  };

  getQuestions(readOnly, row) {
    let questions: QuestionBase<any>[] = [

      new LabelQuestion({
        value: row.name,
        key: "label"
      }),

      new TextareaQuestion({
        readOnly: readOnly,
        value: row.surname,
        key: "textArea"
      })
     ];
    return questions;
  }
}
