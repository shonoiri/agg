import { BrowserModule }                from '@angular/platform-browser';
import { ReactiveFormsModule }          from '@angular/forms';
import { NgModule }                     from '@angular/core';

import { DataDrivenComponent }          from './data_driven.component';
import { DynamicFormQuestionComponent } from './data_driven_form_question.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [ BrowserModule, ReactiveFormsModule, MatExpansionModule, BrowserAnimationsModule ],
  declarations: [ DataDrivenComponent, DynamicFormQuestionComponent ],
  bootstrap: [ DataDrivenComponent ]
})
export class DataDrivenModule {
  constructor() {
  }
}
