import { Component, Input, OnInit }  from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from '@angular/common/http';
import { FormControl, FormGroup, Validators,  AbstractControl, FormBuilder } from '@angular/forms';
import { QuestionService } from './question.service';
import { QuestionBase }     from './model/question_base';
import { Department } from '../model/department';

@Component({
  selector: 'ddc',
  templateUrl: './data_driven.component.html',
  providers:  [QuestionService],
  styleUrls: ['./data_driven.component.css']
})
export class DataDrivenComponent {
  components = [];
  @Input() form: FormGroup;
  @Input() customForm: FormGroup;


  constructor(service: QuestionService, private http: HttpClient, private fb: FormBuilder) {
    this.form = this.fb.group({});
    this.form.addControl("custom", this.fb.group({"select": new FormControl("")}));
    this.http
      .get<Department[]>(
        "http://localhost:8080/agg/component/",
        {
          headers: new HttpHeaders({
          'Authorization': 'Basic dGVzdDp0ZXN0',
          'Content-Type':  'application/json'
          })
        }
      ).subscribe(data => {
        data.forEach(
          department => {

            let component =
                    {
                      name: department.code,
                      rows: []
                    };
            let group = this.fb.group({});
            department.employees.forEach(
              (employee, index) => {
                let row = {
                  info : [],
                  pre : [],
                  post : []
                 };
                let info = this.fb.group({});
                let questions = service.getQuestions(false, employee);
                this.addQuestions(info, questions);
                row.info = questions;
                group.addControl("info" + index, info);

                let pre = this.fb.group({});
                questions = service.getPreQuestions(false, employee);
                this.addQuestions(pre, questions);
                row.pre = questions;
                group.addControl("preview" + index, pre);

                let post = this.fb.group({});
                questions = service.getPostQuestions(false, employee);
                this.addQuestions(post, questions);
                row.post = questions;
                group.addControl("review" + index, post);

                component.rows.push(row);
              });
            this.form.addControl(department.code, group);
            this.components.push(component);
          }
        );
      });
        this.customForm = <FormGroup> this.form.get("custom");
        console.log(this.form);
  }

  addQuestions(group, questions){
    questions.forEach(question => {
      group.addControl(question.key,  question.required ? new FormControl(question.value || '',  Validators.required)
                                              : new FormControl(question.value || ''));
    });
  }

  resetValues(group){
    Object.keys(group.controls).forEach(key => {
      group.controls[key].setValue("");
    });
  }

  updateValues(targetGroup, srcGroup){
    Object.keys(targetGroup.controls).forEach(key => {
      targetGroup.controls[key].setValue(srcGroup.get(key).value);
    });
  }

  copy() {
    this.components.forEach(
      component => {
        let rootForm = this.form.controls[component.name];
        for (let i = 0; i < component.rows.length; i++) {
          this.updateValues(rootForm.get('review' + i), rootForm.get('preview' + i));
        }
      }
    );
  }

  reset() {
    this.customForm.get("select").setValue("");
    this.components.forEach(
      component => {
        let rootForm = this.form.controls[component.name];
        for (let i = 0; i < component.rows.length; i++) {
          this.resetValues(rootForm.get('review' + i));
        }
      }
    );
  }

  save() {
    JSON.stringify(this.form.value);
  }
}
