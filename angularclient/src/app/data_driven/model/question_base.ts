export class QuestionBase<T> {
  value: T;
  values: T[];
  key: string;
  readOnly: boolean;
  maxValue: number;
  minValue: number;
  required: boolean;
  controlType: string;

  constructor(options: {
      value?: T,
      values?: T[],
      key?: string,
      required?: boolean,
      maxValue?: number,
      minValue?: number,
      controlType?: string,
      readOnly?: boolean
    } = {}) {
    this.value = options.value;
    this.values = options.values;
    this.key = options.key || '';
    this.required = !!options.required;
    this.readOnly = !!options.readOnly;
    this.maxValue = options.maxValue;
    this.minValue = options.minValue;
    this.controlType = options.controlType || '';
  }
}
