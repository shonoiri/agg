import { QuestionBase } from './question_base';

export class LabelQuestion extends QuestionBase<string> {
  controlType = 'label';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
