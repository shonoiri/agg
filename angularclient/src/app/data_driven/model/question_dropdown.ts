import { QuestionBase } from './question_base';

export class DropdownQuestion extends QuestionBase<string> {
  controlType = 'dropdown';
  options: string[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['values'] || [];
  }
}
