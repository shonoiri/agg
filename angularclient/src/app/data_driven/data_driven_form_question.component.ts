import { Component, Input } from '@angular/core';
import { FormGroup }        from '@angular/forms';

import { QuestionBase }     from './model/question_base';

@Component({
  selector: 'app-question',
  templateUrl: './data_driven_form_question.component.html',
  styleUrls: ['./data_driven.component.css']
})
export class DynamicFormQuestionComponent {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.question.key].valid;}

  onSelectChange() {
    let value = this.form.controls[this.question.key].value;
    if(value == "admin")
      this.form.parent.parent.get("custom").get("select").setValue(value);
  }

}
