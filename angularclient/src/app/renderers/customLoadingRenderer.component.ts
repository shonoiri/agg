import { Component } from '@angular/core';
import { ICellRenderer } from "ag-grid";

@Component({
    selector: 'app-loading-renderer',
    template: `<div class="ag-custom-loading-cell" style="padding-left: 10px; line-height: 25px;">` +
              `   <i class="fas fa-spinner fa-pulse"></i> <span> {{this.params.loadingMessage}} </span>` +
              `</div>`
})
export class CustomLoadingRenderer implements ICellRenderer {

    params: any;

    agInit(params): void {
        this.params = params;
    }

    refresh(params){
      return true;
    }
}
