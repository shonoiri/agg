import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InlineComponent} from './inline/inline.component';
import {TreeComponent} from './tree/tree.component';
import { DataDrivenComponent } from './data_driven/data_driven.component';

const routes: Routes = [
  {
    path: 'tree',
    component: TreeComponent
  },
  {
    path: 'inline',
    component: InlineComponent
  },
  {
    path: 'data_driven',
    component: DataDrivenComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
  {
          enableTracing: false, // <-- debugging purposes only
        })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
