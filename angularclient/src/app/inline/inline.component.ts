import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';
import { Row } from "../model/row";
import "ag-grid-enterprise";
import { Component, ViewEncapsulation } from "@angular/core";
import { TextAreaEditor } from "../editors/textAreaEditor.component";
import { NumericEditor } from "../editors/numericEditor.component";
import { CustomLoadingRenderer } from "../renderers/customLoadingRenderer.component";
import { PartialMatchFilter } from "../filters/text_partial_match_filter.component";

@Component({
	selector: 'inline',
	templateUrl: './inline.component.html',
	styleUrls: ['inline.component.css'],
	encapsulation: ViewEncapsulation.None
})
export class InlineComponent {
	private gridApi;
	private gridColumnApi;
	private dataToUpdate = [];

    private datasource;
	rowModelType;

    frameworkComponents;
    loadingCellRenderer;
    loadingCellRendererParams;
	rowData: [];

	columnDefs = [
	    {
			field: "name",
			filter: 'partialMatchFilter',
			filterParams: {
              clearButton: true,
              applyButton: true,
              debounceMs: 2000
            }
		},
		{
			field: "code",
			filter: 'partialMatchFilter'
		},
		{
			field: "address",
			cellEditorFramework: TextAreaEditor,
			cellEditorParams: {
				maxLength: 400
			},
			filter: 'partialMatchFilter',
			autoHeight: true
		},
		{
			field: "supervisor",
			filter: 'partialMatchFilter'
		},
		{
			field: "activity",
			cellEditor: "agSelectCellEditor",
			cellEditorParams: {
				values: ["IT",
					"ACCOUNTING",
					"MANAGEMENT",
					"MAINTENANCE",
					"SALES"
				]
			},
			filter: 'partialMatchFilter'
		},
		{
			field: "employeeCount",
			cellEditorFramework: NumericEditor,
			filter: 'partialMatchFilter'
		}
	];

	defaultColDef = {
		sortable: true,
		resizable: true,
		filter: true,
		editable: true
	};

	constructor(private http: HttpClient) {
        this.rowModelType = "serverSide";
        this.frameworkComponents = { customLoadingRenderer: CustomLoadingRenderer, partialMatchFilter: PartialMatchFilter };
        this.loadingCellRenderer = "customLoadingRenderer";
        this.loadingCellRendererParams = { loadingMessage: "One moment please..." };
        this.datasource = {
          getRows(params) {
            let sortModel = params.request.sortModel;
            let sort = '';

            if(sortModel[0])
              sort = '&sort=' + sortModel[0].colId + ',' + sortModel[0].sort;
            const urlService = 'http://localhost:8080/agg/department/rowsFullView/?page=' + (params.request.startRow/100) + sort;

            let method;
            let filterModel = params.request.filterModel;
            let filterPresent = filterModel && Object.keys(filterModel).length > 0;

            if(filterPresent) {
              let filter;
              if(filterModel.name)
                filter = {name : filterModel.name.value};
              if(filterModel.code)
                filter = {code : filterModel.code.value};
              if(filterModel.address)
                filter = {address : filterModel.address.value};

              method = {
                method: "POST",
                body : JSON.stringify(filter),
                headers: {
                  'Authorization': 'Basic dGVzdDp0ZXN0',
                  'Content-Type': 'application/json'
                }
              };
            } else
              method = {
                method: "GET",
                headers: {
                  'Authorization': 'Basic dGVzdDp0ZXN0'
                }
              };

              fetch(urlService, method)
              .then(res => res.json())
              .then(rowData => {
                let page = rowData.page;
                if (page.totalElements > 0) {
                  let lastRow = () => {
                    if (page.totalPages <= 1) return page.totalElements;
                    else if (params.request.endRow <= page.totalPages - 2) return -1;
                    else return page.totalElements;
                  };
                  params.successCallback(rowData._embedded.departmentEntities, lastRow());
                } else {
                  params.successCallback([{columnNameField: "No results found"}], 1);
                }
              })
              .catch(error => console.error("Error: ", error))
          }
        };
	}

    httpOptions = {
        headers: new HttpHeaders({
            'Authorization': 'Basic dGVzdDp0ZXN0'
        })
    }

	save() {
		this.http.post(
			"http://localhost:8080/agg/department/updateRows",
			this.dataToUpdate,
			this.httpOptions
		).subscribe();
		this.dataToUpdate = [];
		this.gridApi.refreshCells();
	}

	update() {
		this.gridApi.refreshCells();
	}

	onCellValueChanged(event) {
		if (event.newValue != event.oldValue) {
			let data = event.node.data;
            let department = {
                name : data.name,
                id : data.id
            };
		    this.dataToUpdate.push(department);
		}
		if(event.column.colId == "address") {
		    event.node.setRowHeight(28);
		}
	}

	getRowData(node) {
		while (node.group) {
			node = node.childrenAfterGroup[0];
		}
		return node.data;
	}

	onGridReady(params) {
		this.gridApi = params.api;
		this.gridColumnApi = params.columnApi;
        this.gridApi.setServerSideDatasource(this.datasource);
        this.gridApi.sizeColumnsToFit()
	}
}

