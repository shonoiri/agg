import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { InlineComponent }   from './inline.component';
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TextAreaEditor } from "../editors/textAreaEditor.component";
import { NumericEditor } from "../editors/numericEditor.component";
import { CustomLoadingRenderer } from "../renderers/customLoadingRenderer.component";
import { PartialMatchFilter } from "../filters/text_partial_match_filter.component";

@NgModule({
	imports: [
	  CommonModule,
		FormsModule,
		AgGridModule.withComponents([TextAreaEditor, NumericEditor, CustomLoadingRenderer, PartialMatchFilter]),
	],
	declarations: [
		InlineComponent,
		TextAreaEditor,
		NumericEditor,
		CustomLoadingRenderer,
		PartialMatchFilter
	]
})
export class InlineModule {}
