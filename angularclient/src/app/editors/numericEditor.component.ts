import {AfterViewInit, Component, ViewChild, ViewContainerRef} from "@angular/core";
import {ICellEditorAngularComp} from "ag-grid-angular";
import {Constants} from "ag-grid";

@Component({
    selector: 'numeric-cell',
    template: `<input #input (keydown)="onKeyDown($event)" class="ag-cell-edit-input" style="width: 100%; height: 100%; font: inherit !important;" [(ngModel)]="value">`
})
export class NumericEditor implements ICellEditorAngularComp, AfterViewInit {
    private params: any;
    public value: number;

    @ViewChild('input', {read: ViewContainerRef, static: true}) public input;

    ngAfterViewInit() {
      window.setTimeout(() => {
        this.input.element.nativeElement.focus();
      })
    }

    agInit(params: any): void {
        this.params = params;
        this.value = this.params.value;
    }

    getValue(): any {
        return this.value;
    }

    onKeyDown(event): void {
        const charCode = this.getCharCodeFromEvent(event);
        const charStr = event.key ? event.key : String.fromCharCode(charCode);

        if (!this.isCharNumeric(charStr)
            && charCode != Constants.KEY_DELETE
            && charCode != Constants.KEY_BACKSPACE
            && charCode != Constants.KEY_LEFT
            && charCode != Constants.KEY_RIGHT) {
            if (event.preventDefault) event.preventDefault();
        }
    }

    private getCharCodeFromEvent(event): any {
        event = event || window.event;
        return (typeof event.which == "undefined") ? event.keyCode : event.which;
    }

    private isCharNumeric(charStr): boolean {
        return !!/\d/.test(charStr);
    }
}
