import {AfterViewInit, Component, ViewChild, ViewContainerRef} from "@angular/core";
import {ICellEditorAngularComp} from "ag-grid-angular";
import {Constants} from "ag-grid";

@Component({
	selector: 'editor-cell',
	templateUrl: './textAreaEditor.component.html',
})

export class TextAreaEditor implements ICellEditorAngularComp, AfterViewInit  {
	private params: any;
    text: string = "";

	@ViewChild('input', {read: ViewContainerRef, static: true})
	public input;

    ngAfterViewInit() {
      window.setTimeout(() => {
        this.input.element.nativeElement.focus();
      })
    }

    agInit(params: any): void {
        this.params = params;
        this.setText(params.value);
        this.params.eGridCell.style.setProperty("height", "inherit");
        this.input.element.nativeElement.maxLength = params.maxLength ? params.maxLength : 200;
        this.params.node.setRowHeight(28 * (Math.floor(params.value.length / 32) + 1));
    }

    getValue(): any {
        return this.text;
    }

    isPopup(): boolean {
        return false;
    }

    setText(text : string): void {
        this.text = text;
    }

    onKeyDown(event): void {
        this.params.node.setRowHeight(28 * (Math.floor(event.target.value.length / 32) + 1));
		    let key = event.which || event.keyCode;
		    if (key == Constants.KEY_LEFT ||
			      key == Constants.KEY_UP ||
			      key == Constants.KEY_RIGHT ||
			      key == Constants.KEY_DOWN ||
			      (event.shiftKey && key == Constants.KEY_ENTER))
				      event.stopPropagation();
    }
}

