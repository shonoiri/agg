import { Employee } from './employee';

export class Department {
  id : number;
  name : string;
  code : string;
  employees: Employee[];
}
