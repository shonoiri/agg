import {Employee} from "./Employee";
import {Department} from "./Department";
import {Task} from "./Task";

export class Row {
  department : Department;
  employee : Employee;
  task : Task
}
