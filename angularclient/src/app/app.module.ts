import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AgGridModule} from 'ag-grid-angular';
import {HttpClientModule} from "@angular/common/http";
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import {InlineModule} from './inline/inline.module';
import {TreeModule} from './tree/tree.module';
import { AppRoutingModule } from './app-routing.module';
import { DataDrivenModule } from './data_driven/data_driven.module';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    ReactiveFormsModule,
    AppRoutingModule,
    InlineModule,
    TreeModule,
    DataDrivenModule
  ],
  declarations: [
      AppComponent
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
