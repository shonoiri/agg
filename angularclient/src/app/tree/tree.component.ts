import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from '@angular/common/http';
import {Row} from "../model/row";
import "ag-grid-enterprise";

@Component({
  selector: 'tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent{
  private gridApi;
  private gridColumnApi;
  private dataToUpdate;

  columnDefs;
  defaultColDef;

  rowData: any;

  constructor(private http: HttpClient) {
    this.dataToUpdate = [];
    this.columnDefs = [
      {
        headerName: "Department",
        showRowGroup: "department",
        cellRenderer: "agGroupCellRenderer",
        cellRendererParams: {
          suppressCount : true,
        },
        valueSetter : departmentNameValueSetter,
        editable: function(params) {
          return params.node.field == 'department';
        }
      },
      {
        headerName: "Employee",
        showRowGroup: "employee",
        cellRenderer: "agGroupCellRenderer",
        cellRendererParams: {
          suppressCount : true
        },
        rowDrag: function(params) {
          return params.node.field == 'employee';
        },
        valueSetter : employeeNameValueSetter,
        editable: function(params) {
          return params.node.field == 'employee';
        }
      },

      {
        field: "department",
        rowGroup: true,
        keyCreator: function(params) {
          return params.value.name;
        },
        hide: true,
        suppressMenu : true,
        suppressToolPanel: true
      },
      {
        field: "employee",
        rowGroup: true,
        keyCreator: function(params) {
          if(params.value)
            return params.value.name + ' ' + params.value.surname;
        },
        hide: true,
        suppressToolPanel: true
      },
      {
        field: "task.title",
        rowDrag: function(params) {
          return params.node.data && params.node.data.task && !params.node.group;
        },
        editable: function(params) {
                  return !params.node.group;
                }
      } ,
      {
        field: "task.description",
        editable: function(params) {
                          return !params.node.group;
                        }
      } ,
      {
        field: "task.created",
        editable: function(params) {
                          return !params.node.group;
                        }
      },

    ];
    this.http
      .get(
        "http://localhost:8080/agg/department/rowsGroupingView",
      ).subscribe(data => {this.rowData = data});

    this.defaultColDef = {
        sortable: true,
        resizable: true,
        filter: true
    };
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
  }

  onRowDragEnd(event) {
    let nodeToMove = event.node;
    let targetNode = event.overNode;
    let targetNodeData = this.getRowData(targetNode);
    let targetDepartment = targetNodeData.department;
    let targetEmployee = targetNodeData.employee;

    if (nodeToMove.parent.childrenAfterGroup.length <= 1) {
      let nodeToMoveData = this.getRowData(nodeToMove);
      let addData = {
        department : nodeToMoveData.department,
        employee : null,
        task : null
      };
      if(!nodeToMove.group)
        addData.employee = nodeToMoveData.employee;
      this.gridApi.updateRowData({add: [addData]});
    }

    if(nodeToMove.group){
      nodeToMove.childrenAfterGroup.forEach (
        c => {
          c.data.department = targetDepartment;
          this.gridApi.batchUpdateRowData({update: [c.data]});
        }
      )
    } else if(targetNode.field === 'employee') {
      nodeToMove.data.department = targetDepartment;
      nodeToMove.data.employee = targetEmployee;
      this.gridApi.updateRowData({update: [nodeToMove.data]});
    }

    this.gridApi.clearFocusedCell();
  }

  save(){
    this.http.post(
            "http://localhost:8080/agg/department/updateRows",
            this.dataToUpdate
          ).subscribe();
    this.http
          .get(
            "http://localhost:8080/agg/department/rowsGroupingView",
          ).subscribe(data => {this.rowData = data});
    this.gridApi.redrawRows();
    this.dataToUpdate = [];
  }

  onCellValueChanged(event) {
    let node = event.node;
    let data = node.data;
    if(node.group)
      data = node.allLeafChildren[0].data
    let row;
    if(node.field === 'department') {
      row = {
        department : data.department,
        employee : null,
        task : null
      }
    } else if(node.field === 'employee') {
       row = {
        department : null,
        employee : data.employee,
        task : null
       }
    } else
      row = data;
    this.dataToUpdate.push(row);
  }

  getRowData(node){
    while (node.group) {
      node = node.childrenAfterGroup[0];
    }
    return node.data;
  }

}
function employeeNameValueSetter(params) {
    let fullName = params.newValue;
    let nameSplit = fullName.split(" ");
    let newFirstName = nameSplit[0];
    let newLastName = nameSplit[1];
    let node = params.node;
    if(fullName !== params.oldValue) {
      node.groupData[1] = fullName;
      node.childrenAfterGroup[0].groupData[1] = fullName;
      node.childrenAfterGroup.forEach(
        c => {
          c.data.employee.name = newFirstName;
          c.data.employee.surname = newLastName;
        }
      );
    } else
      return false;
    return true;
  }

function departmentNameValueSetter(params) {
  let departmentName = params.newValue;
  let departmentNameOld = params.oldValue;
  let node = params.node;

  if(departmentNameOld !== departmentName) {
    node.groupData[0] = departmentName;
    node.key = departmentName;
    node.childrenAfterGroup[0].groupData[0] = departmentName;
    node.allLeafChildren[0].groupData[0] = departmentName;
    node.allLeafChildren.forEach(
      c => {
        c.data.department.name = departmentName;
      }
    );
  } else
    return false;
  return true;
}
