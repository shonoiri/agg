import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { TreeComponent }   from './tree.component';
import {AgGridModule} from 'ag-grid-angular';

@NgModule({
  imports: [CommonModule,
  AgGridModule.withComponents([]),],
  declarations: [
    TreeComponent
  ]
})
export class TreeModule {}
