import {Component, ViewChild, ViewContainerRef} from "@angular/core";

import {IAfterGuiAttachedParams, IDoesFilterPassParams, IFilterParams, RowNode} from "ag-grid-community";
import {IFilterAngularComp} from "ag-grid-angular";

@Component({
    selector: 'filter-cell',
    template: `
        <div class="container">
            <input #input (ngModelChange)="onChange($event)" [ngModel]="text" style="width:100%">
        </div>
    `, styles: [
        `
            .container {
                padding : 10px
            }
        `
    ]
})
export class PartialMatchFilter implements IFilterAngularComp {
    private params: IFilterParams;
    private valueGetter: (rowNode: RowNode) => any;
    public text: string = '';

    @ViewChild('input', {read: ViewContainerRef, static: true}) public input;

    agInit(params: IFilterParams): void {
        console.log(params);
        this.params = params;
        this.valueGetter = params.valueGetter;
        setTimeout(this.onChange, 2000);
    }

    isFilterActive(): boolean {
        return this.text !== null && this.text !== undefined && this.text !== '';
    }

    doesFilterPass(params: IDoesFilterPassParams): boolean {
        return this.text.toLowerCase()
            .split(" ")
            .every((filterWord) => {
                return this.valueGetter(params.node).toString().toLowerCase().indexOf(filterWord) >= 0;
            });
    }

    getModel(): any {
        return {value: this.text};
    }

    setModel(model: any): void {
        this.text = model ? model.value : '';
    }

    ngAfterViewInit(params: IAfterGuiAttachedParams): void {
        window.setTimeout(() => {
            this.input.element.nativeElement.focus();
        })
    }

    componentMethod(message: string): void {
        alert(`Alert from PartialMatchFilterComponent ${message}`);
    }

    onChange(newValue): void {
        if (this.text !== newValue) {
            this.text = newValue;
            this.params.filterChangedCallback();
        }
    }
}
