package com.cgi.agg.features.steps;

import com.cgi.agg.features.steps.serenity.UserSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;

public class EditRowStepDefinitions {
    @Steps
    UserSteps user;

    @Test
    @When("I change department {string} to {string}")
    public void editDepartmentName(String column, String value) {
        user.edits_cell_value_to(column, value);
    }

    @Test
    @And("click {string} button")
    public void saveChanges(String button) {
        user.clicks_on_button(button);
    }

    @Test
    @Then("the {string} of department should be {string}")
    public void editResult(String column, String value) {
        user.should_see_cell_value_changed_to(column, value);
    }
}
