package com.cgi.agg.features.steps.serenity;

import com.cgi.agg.pages.HomePage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.List;

public class UserSteps {
    HomePage homePage;

    @Step
    public void opens_home_page() {
/*        InternetExplorerOptions options = new InternetExplorerOptions();
        options.requireWindowFocus();
        System.setProperty("webdriver.ie.driver", "src\\test\\resources\\IEDriverServer.exe");
        homePage.setDriver(new InternetExplorerDriver(options));*/
        homePage.open();
    }

    @Step
    public void searches_for_items_containing(String keywords) {
        homePage.searchFor(keywords);
    }

    @Step
    public void should_see_items_related_to(String keywords) {
        List<String> resultTitles = homePage.getResultTitles();
        resultTitles.stream().forEach(title -> Assert.assertTrue(title + " dose not contains " + keywords,
               title.isEmpty() || title.contains(keywords)));
    }

    @Step
    public void edits_cell_value_to(String column, String value) {
        homePage.editValue(column, value);
    }

    @Step
    public void clicks_on_button(String button) {
        homePage.clickOnButton(button);
    }

    @Step
    public void should_see_cell_value_changed_to(String column, String value) {
        String cellValue = homePage.getValue(column);
        Assert.assertTrue(cellValue + " dose not contains " + value,
                cellValue.contains(value));
    }
}
