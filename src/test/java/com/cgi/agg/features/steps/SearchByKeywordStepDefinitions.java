package com.cgi.agg.features.steps;

import com.cgi.agg.features.steps.serenity.UserSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;

public class SearchByKeywordStepDefinitions {
    @Steps
    UserSteps user;

    @Test
    @Given("I am on the Inline page")
    public void buyerWantsToBuy() {
        user.opens_home_page();
    }

    @Test
    @When("I search for {string} department")
    public void searchByKeyword(String keyword) {
        user.searches_for_items_containing(keyword);
    }

    @Test
    @Then("I should only see items related to {string}")
    public void searchResults(String keyword) {
        user.should_see_items_related_to(keyword);
    }

}
