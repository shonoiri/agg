package com.cgi.agg;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"},
                    glue = {"classpath:com.cgi.agg.features.steps"},
                    features = "src/test/resources/features")
public class RunCucumberTest{}
