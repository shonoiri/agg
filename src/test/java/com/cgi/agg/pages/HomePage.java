package com.cgi.agg.pages;

import cucumber.api.java.Before;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.webdriver.javascript.JavascriptExecutorFacade;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("http://localhost:4200/inline")
public class HomePage extends PageObject {

    @FindBy(tagName = "ag-grid-angular")
    WebElement main;

    @FindBy(tagName = "inline")
    WebElement page;

    public void clickOnButton(String button) {
        Actions actions = new Actions(getDriver());
        actions.click(main).build().perform();
        WebElement btn = page.findElement(org.openqa.selenium.By.tagName("form"))
                                .findElement(org.openqa.selenium.By.tagName("button"));
        actions.moveToElement(btn).click(btn).build().perform();
    }

    public void editValue(String column, String value) {
        Actions actions = new Actions(getDriver());
        WebElement body = main.findElement(By.cssSelector("div[ref=\"eCenterContainer\"]"));
        WebElement row = body.findElement(By.cssSelector("div[row-id='0']"));
        WebElement cell = row.findElement(By.cssSelector("div[col-id='" + column + "']"));
        actions.moveToElement(cell).doubleClick(cell).build().perform();
        WebElement cellInput = cell.findElement(By.className("ag-cell-edit-input"));
        if(cellInput.getTagName().equals("div"))
            cellInput = cellInput.findElement(By.className("ag-cell-edit-input"));

        if(cellInput.getTagName().equals("input") || cellInput.getTagName().equals("textarea"))
            actions.sendKeys(cellInput, value).build().perform();
        else if(cellInput.getTagName().equals("select")){
            Select select = new Select(cellInput);
            select.selectByValue(value);
        }
    }

    public String getValue(String column) {
        WebElement body = main.findElement(By.cssSelector("div[ref=\"eCenterContainer\"]"));
        WebElement row = body.findElement(By.cssSelector("div[row-id='0']"));
        WebElement cell = row.findElement(By.cssSelector("div[col-id='" + column + "']"));
        return cell.getText();
    }

    public void searchFor(String keywords) {
        Actions actions = new Actions(getDriver());
        WebElement headerRow = main.findElement(By.cssSelector("div[role='row']"));
        WebElement nameColumn = headerRow.findElement(By.cssSelector("div[col-id=\"name\"]"));
        WebElement menu = nameColumn.findElement(By.cssSelector("span[ref=\"eMenu\"]"));
        actions.moveToElement(menu).click().build().perform();
        List<WebElement> menuTabs = main.findElements(By.cssSelector("span[class=\"ag-tab\"]"));
        actions.moveToElement(menuTabs.get(0)).click().perform();
        WebElement body = main.findElement(By.cssSelector("div[ref=\"tabBody\"]"));
        WebElement filter = body.findElement(By.tagName("filter-cell"));
        actions.sendKeys(filter, keywords).build().perform();
    }

    public List<String> getResultTitles(){
            WebElement body = main.findElement(By.cssSelector("div[ref='eCenterColsClipper']"));
            List<WebElement> rows = body.findElements(By.cssSelector("div[role='row']"));
            return rows.stream().map(row -> row.findElement(By.cssSelector("div[col-id='name']"))
                    .getText()).collect(Collectors.toList());
    }
}
