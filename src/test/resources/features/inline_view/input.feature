Feature: Edit  row

  Scenario: Edited textInput should be saved
    Given I am on the Inline page
    When I change department "name" to "Test Value"
    And I change department "address" to "Test Value Address"
    And I change department "employeeCount" to "150"
    And I change department "activity" to "ACCOUNTING"
    And click "Save" button
    Then the "name" of department should be "Test Value"
    And the "address" of department should be "Test Value Address"
    And the "employeeCount" of department should be "150"
    And the "activity" of department should be "ACCOUNTING"
