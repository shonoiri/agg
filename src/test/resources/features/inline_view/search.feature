Feature: Searching by keyword

  Scenario: Should list items related to a specified keyword
    Given I am on the Inline page
    When I search for "Dep" department
    Then I should only see items related to "Dep"