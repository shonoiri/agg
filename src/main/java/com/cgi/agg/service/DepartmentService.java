package com.cgi.agg.service;

import com.cgi.agg.entity.DepartmentEntity;
import com.cgi.agg.vo.Department;
import com.cgi.agg.vo.DepartmentExtended;
import com.cgi.agg.vo.Row;
import com.cgi.agg.vo.RowExtended;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface DepartmentService {

    List<Department> getAll();

    List<Row> getTreeRows();

    Page<DepartmentEntity> getFullRows(Pageable pageable);

    Page<DepartmentEntity> getFullRows(DepartmentEntity entity, Pageable pageable);

    DepartmentExtended getById(int id);

    void updateRows(Set<RowExtended> rows) throws NotFoundException;

    void delete(int id);

    void create(Department department);
}
