package com.cgi.agg.service;

import com.cgi.agg.dao.ComponentRepository;
import com.cgi.agg.entity.DepartmentEntity;
import com.cgi.agg.vo.Department;
import com.cgi.agg.vo.DepartmentExtended;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ComponentServiceImpl implements ComponentService {

    @Autowired
    private ComponentRepository repository;

    private ModelMapper modelMapper = new ModelMapper();

    @Override
    public List<DepartmentExtended> getAll() {
        List<DepartmentEntity> departments = (List<DepartmentEntity>) repository.findAll();
        return departments.stream().map(d -> modelMapper.map(d, DepartmentExtended.class)).collect(Collectors.toList());
    }
}
