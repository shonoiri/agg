package com.cgi.agg.service;

import com.cgi.agg.vo.DepartmentExtended;

import java.util.List;

public interface ComponentService {

    List<DepartmentExtended> getAll();
}