package com.cgi.agg.service;

import com.cgi.agg.dao.DepartmentPagingRepository;
import com.cgi.agg.dao.DepartmentRepository;
import com.cgi.agg.dao.EmployeeRepository;
import com.cgi.agg.dao.TaskRepository;
import com.cgi.agg.entity.DepartmentEntity;
import com.cgi.agg.entity.EmployeeEntity;
import com.cgi.agg.entity.TaskEntity;
import com.cgi.agg.vo.*;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository repository;

    @Autowired
    private DepartmentPagingRepository repositoryPaging;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TaskRepository taskRepository;

    private ModelMapper modelMapper = new ModelMapper();

    @Override
    public List<Department> getAll() {
        List<DepartmentEntity> departments = (List<DepartmentEntity>) repository.findAll();
        return departments.stream().map(d -> modelMapper.map(d, Department.class)).collect(Collectors.toList());
    }

    @Override
    public DepartmentExtended getById(int id) {
        DepartmentEntity department = repository.findById(id).orElse(new DepartmentEntity());
        return modelMapper.map(department, DepartmentExtended.class);
    }

    @Override
    public void updateRows(Set<RowExtended> rows) throws NotFoundException{
        System.out.println("update rows service");
        for (RowExtended row : rows) {
            if(row.getEmployee() == null && row.getTask() == null)
                updateDapartment(row.getDepartment());
            else if (row.getEmployee() == null)
                updateEmployee(row.getEmployee());
            else
                updateTask(row.getTask());
        }
    }

    private void updateDapartment(DepartmentExtended department) throws NotFoundException {
        if(!repository.existsById(department.getId()))
            throw new NotFoundException("department not found");
        DepartmentEntity entity = repository.findById(department.getId()).get();
        entity.setName(department.getName());
        entity.setActivity(department.getActivity().value());
        entity.setAddress(department.getAddress());
        entity.setCode(department.getCode());
        entity.setEmployeeCount(department.getEmployeeCount());
        entity.setSupervisor(department.getSupervisor());
        repository.save(entity);
    }

    private void updateEmployee(Employee employee) throws NotFoundException {
        EmployeeEntity entity = employeeRepository.findById(employee.getId()).get();
        if(entity == null)
            throw new NotFoundException("employee not found");
        entity.setName(employee.getName());
        entity.setSurname(employee.getSurname());
        employeeRepository.save(entity);
    }

    private void updateTask(Task task) throws NotFoundException {
        TaskEntity entity = taskRepository.findById(task.getId()).get();
        if(entity == null)
            throw new NotFoundException("task not found");
        entity.setCreated(task.getCreated());
        entity.setDescription(task.getDescription());
        entity.setTitle(task.getTitle());
        taskRepository.save(entity);
    }
    @Override
    public void delete(int id) {

        repository.deleteById(id);
    }

    @Override
    public void create(Department department) {

        repository.save(modelMapper.map(department, DepartmentEntity.class));
    }

    @Override
    public List<Row> getTreeRows() {
        List<Row> rows = new LinkedList<>();
        for (DepartmentEntity department: repository.findAll()) {
            if(department.getEmployees().isEmpty())
                rows.add(new Row(modelMapper.map(department, Department.class)));
            for (EmployeeEntity employee: department.getEmployees()) {
                if(employee.getTasks().isEmpty())
                    rows.add(
                            new Row(
                                modelMapper.map(department, Department.class),
                                modelMapper.map(employee, Employee.class)));
                for (TaskEntity task: employee.getTasks()) {
                    rows.add(
                            new Row(
                                    modelMapper.map(department, Department.class),
                                    modelMapper.map(employee, Employee.class),
                                    modelMapper.map(task, Task.class)
                                    ));
                }
            }
        }
        return  rows;
    }

    @Override
    public Page<DepartmentEntity> getFullRows(DepartmentEntity entity, Pageable pageable) {
        Example<DepartmentEntity> example = Example.of(entity,
                ExampleMatcher.matching()
                        .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                        .withMatcher("name", match -> match.contains())
                        .withMatcher("employeeCount", match -> match.exact())
        );
        return repositoryPaging.findAll(example, pageable);
    }

    @Override
    public Page<DepartmentEntity> getFullRows(Pageable pageable) {
        return repositoryPaging.findAll(pageable);
    }
}
