package com.cgi.agg.controller;

import com.cgi.agg.entity.DepartmentEntity;
import com.cgi.agg.service.DepartmentService;
import com.cgi.agg.vo.*;
import io.swagger.annotations.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("department")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "Department Management System")
@PermitAll
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @ApiOperation(value = "View a list of departments", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping
    public ResponseEntity<List<Department>> getAll(){
        List<Department> departments = departmentService.getAll();
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }

    @ApiOperation(value = " View a list of departments as a tree rows", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })

    @GetMapping("/rowsGroupingView")
    public ResponseEntity<List<Row>> getRowsGrouping(){
        List<Row> rows = departmentService.getTreeRows();
        return new ResponseEntity<>(rows, HttpStatus.OK);
    }

    @GetMapping("/rowsFullView/")
    public HttpEntity<PagedResources<DepartmentEntity>> getRowsFull(@PageableDefault(value=100) Pageable pageable, PagedResourcesAssembler assembler){
        Page page = departmentService.getFullRows(pageable);
        return new ResponseEntity<>(assembler.toResource(page), HttpStatus.OK);
    }

    @PostMapping("/rowsFullView/")
    public HttpEntity<PagedResources<DepartmentEntity>> getRowsFull(@RequestBody DepartmentEntity department,
                                                                    @PageableDefault(value=100) Pageable pageable, PagedResourcesAssembler assembler){
        Page page = departmentService.getFullRows(department, pageable);
        return new ResponseEntity<>(assembler.toResource(page), HttpStatus.OK);
    }

    @PostMapping("/updateRows")
    public void updateRowsGrouping(@RequestBody Set<RowExtended> rows) throws NotFoundException {
        System.out.println("update rows controller");
        departmentService.updateRows(rows);
    }

    @ApiOperation(value = "Get a department by Id")
    @GetMapping("/{id}")
    public ResponseEntity<DepartmentExtended> getById(
            @ApiParam(value = "Department id", required = true)
            @PathVariable("id") int id){
        DepartmentExtended department = departmentService.getById(id);
        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a department")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(
            @ApiParam(value = "Department Id", required = true)
            @PathVariable("id") int id){
        departmentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Add a department")
    @PostMapping
    public ResponseEntity<Void> create(
            @ApiParam(value = "Department object", required = true)
            @Valid @RequestBody Department department){
        departmentService.create(department);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
