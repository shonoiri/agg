package com.cgi.agg.controller;

import com.cgi.agg.service.ComponentService;
import com.cgi.agg.vo.DepartmentExtended;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import java.util.List;

@RestController
@RequestMapping("component")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "Component Management System")
@PermitAll
public class ComponentController {

    @Autowired
    private ComponentService componentService;

    @GetMapping
    public ResponseEntity<List<DepartmentExtended>> getAll(){
        List<DepartmentExtended> departments = componentService.getAll().subList(0,10);
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }
}
