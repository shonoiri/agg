package com.cgi.agg.dao;

import com.cgi.agg.entity.DepartmentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
public interface DepartmentPagingRepository extends PagingAndSortingRepository<DepartmentEntity, Integer>, QueryByExampleExecutor<DepartmentEntity> {
}
