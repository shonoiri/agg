package com.cgi.agg.dao;

import com.cgi.agg.entity.DepartmentEntity;
import com.cgi.agg.entity.EmployeeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Integer> {
}
