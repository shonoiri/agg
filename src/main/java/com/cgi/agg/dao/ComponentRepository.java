package com.cgi.agg.dao;

import com.cgi.agg.entity.DepartmentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
public interface ComponentRepository extends CrudRepository<DepartmentEntity, Integer> {
}

