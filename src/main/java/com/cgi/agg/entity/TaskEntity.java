package com.cgi.agg.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "task")
@Getter@Setter
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    @NotNull @Size(min = 3, max = 50)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "created")
    @NotNull @PastOrPresent
    private LocalDate created;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private EmployeeEntity employee;

}
