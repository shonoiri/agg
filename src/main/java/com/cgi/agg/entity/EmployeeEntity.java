package com.cgi.agg.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "employee")
@Getter@Setter
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @NotNull @Size(min = 3, max = 30)
    String name;

    @Column(name = "surname")
    @NotNull @Size(min = 3, max = 30)
    String surname;

    @Valid
    @ManyToOne
    @JoinColumn(name = "department_id")
    private DepartmentEntity department;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Setter(AccessLevel.NONE)@Getter(AccessLevel.NONE)
    private List<TaskEntity> tasks;

    public List<TaskEntity> getTasks() {
        if(tasks == null)
            return Collections.unmodifiableList(Collections.emptyList());
        return Collections.unmodifiableList(tasks);
    }

    public void addTask(TaskEntity task) {
        if(tasks == null)
            tasks = new LinkedList<>();
        tasks.add(task);
    }
}
