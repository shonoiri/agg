package com.cgi.agg.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "department")
@Getter@Setter
public class DepartmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    @NotNull @Size(min = 3, max = 30)
    String name;

    @Column(name = "code")
    @NotNull @Size(min = 3, max = 30)
    private String code;
    @Column(name = "company")
    @NotNull @Size(min = 3, max = 30)
    private String company;
    @Column(name = "supervisor")
    @NotNull @Size(min = 3, max = 30)
    private String supervisor;
    @Column(name = "address")
    @NotNull @Size(min = 3)
    private String address;
    @Column(name = "employee_count")
    @NotNull
    private Integer employeeCount;
    @Column(name = "activity")
    @NotNull
    private String activity;

    @Valid
    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Setter(AccessLevel.NONE)@Getter(AccessLevel.NONE)
    private List<EmployeeEntity> employees;

    public List<EmployeeEntity> getEmployees() {
        if(employees == null)
            return Collections.unmodifiableList(Collections.emptyList());
        return Collections.unmodifiableList(employees);
    }

    public void addEmployee(EmployeeEntity employee){
        if(employees == null)
            employees = new LinkedList<>();
        employees.add(employee);
    }
}
