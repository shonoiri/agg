package com.cgi.agg;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class TestDataGenerator {

    public static void main(String[] args) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("testDepartments.csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print("id,name,code,company,employee_count,supervisor,address,activity");

        for (int i = 0; i <= 10020; i++)  {
            writer.println();
            writer.print(i + ",name" + i + ",co" + i + "de," + "company" + i/32 + "," + i/10 + ",superv," + "address" + i/10 + ",");
            if(i%10 == 0)
                writer.print("IT");
            else if(i%3 == 0)
                writer.print("MANAGEMENT");
            else if(i%7 == 0)
                writer.print("SALES");
                //IT, ACCOUNTING, MANAGEMENT, MAINTENANCE, SALES
            else
                writer.print("ACCOUNTING");

        }
        writer.close();
    }
}
