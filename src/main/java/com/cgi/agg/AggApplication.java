package com.cgi.agg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.cgi.agg.dao")
public class AggApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AggApplication.class, args);
    }

}