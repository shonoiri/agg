package com.cgi.agg.vo;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class Input {
    private String value;
    private String[] values;

}
