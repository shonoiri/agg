package com.cgi.agg.vo;

import lombok.Data;

@Data
public class DepartmentExtended extends Department{
    public enum DepartmentActivity {
       IT("IT"),
        ACCOUNTING("ACCOUNTING"),
        MANAGEMENT("MANAGEMENT"),
        MAINTENANCE("MAINTENANCE"),
        SALES("SALES");
        private final String value;

        DepartmentActivity(String val) {
            this.value = val;
        }
        public String value() {
            return value;
        }
    }

    private String code;
    private String company;
    private String supervisor;
    private String address;
    private int employeeCount;
    private DepartmentActivity activity;
}
