package com.cgi.agg.vo;

import lombok.Data;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Data
public class Node {
    private List<String> parents = new LinkedList<>();
    private String title;
    private String description;
    private LocalDate created;

}
