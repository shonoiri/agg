package com.cgi.agg.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data@NoArgsConstructor
public class RowExtended {

    public RowExtended(DepartmentExtended department){
        this.department = department;
    }

    public RowExtended(DepartmentExtended department, Employee employee){
        this(department);
        this.employee = employee;
    }

    public RowExtended(DepartmentExtended department, Employee employee, Task task){
        this(department, employee);
        this.task = task;
    }

    private DepartmentExtended department;
    private Employee employee;
    private Task task;
}
