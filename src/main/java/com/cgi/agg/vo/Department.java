package com.cgi.agg.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data@NoArgsConstructor@AllArgsConstructor
public class Department {

    private int id;
    private String name;
    private List<Employee> employees;
}
