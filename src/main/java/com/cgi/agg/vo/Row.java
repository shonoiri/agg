package com.cgi.agg.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data@NoArgsConstructor
public class Row {

    public Row(Department department){
        this.department = department;
    }

    public Row(Department department, Employee employee){
        this(department);
        this.employee = employee;
    }

    public Row(Department department, Employee employee, Task task){
        this(department, employee);
        this.task = task;
    }

    private Department department;
    private Employee employee;
    private Task task;
}
