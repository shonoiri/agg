AG Grid prototype
============================
### Installation
mvn install

### Run
mvn spring-boot:run

### Root Path
 /agg

### Swagger
http://localhost:8080/agg/swagger-ui.html#
